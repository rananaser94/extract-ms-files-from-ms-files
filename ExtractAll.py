import glob
import os.path
import shutil
import tempfile
from datetime import datetime
import zipfile
import docx
import os
import struct

from pptx import Presentation
from pptx.enum.shapes import MSO_SHAPE_TYPE


def extract_Embeddings_from_pptx(filename):
    prs = Presentation(filename)

    prog_id_ext_map = {
        'Excel.Sheet.8': 'xls',
        'PowerPoint.Show.8': 'ppt',
        'Word.Document.8': 'doc',
        'PowerPoint.Show.12': 'pptx',
        'Word.Document.12': 'docx',
        'Excel.Sheet.12': 'xlsx'

    }
    cnt = 0
    # Iterate through all the slides
    for slide in prs.slides:
        # Iterate through all the shapes(itemes) in each slide
        for shape in slide.shapes:
            # Check if the shape is an embedded object of type
            if str(shape.shape_type) == 'EMBEDDED_OLE_OBJECT (7)':
                # Check if the embedded object is of type to get the extension
                if shape.ole_format.prog_id in prog_id_ext_map.keys():
                    # Create a new directory to store the files
                    os.makedirs('embedded_files', exist_ok=True)
                    # Save the file with the original file name
                    ext = prog_id_ext_map[shape.ole_format.prog_id]
                    # Save the file with the original file name and extension
                    file_name = f"{shape.ole_format.prog_id}_{shape.shape_id}.{ext}"
                    with open('embedded_files/' + file_name, 'wb') as f:
                        f.write(shape.ole_format.blob)
                    cnt += 1


def convert_OldMS(filePath, extension):

    if(extension=='.doc'):
        comObject = "Word.application"
        fileFormate = 16
    elif(extension=='.xls'):
        comObject = "Excel.application"
        fileFormate = 51
    elif(extension=='.ppt'):
        comObject = "PowerPoint.Application"
        fileFormate = 24
    documentNew = win32com.client.Dispatch(comObject)
    fileName = filePath + "x"
    if not os.path.isfile(fileName):  # Skip conversion where docx file already exists

        file_path = os.path.abspath(filePath)
        docx_file = os.path.abspath(fileName)
        try:
            if(extension=='.doc'):
                wordDoc = documentNew.Documents.Open(file_path)
                wordDoc.SaveAs2(docx_file, FileFormat=fileFormate)
                wordDoc.Close()
            elif(extension==".xls"):
                wordDoc = documentNew.Workbooks.Open(file_path)
                wordDoc.SaveAs(docx_file, FileFormat=fileFormate)
                wordDoc.Close()
            elif(extension==".ppt"):
                wordDoc = documentNew.Presentations.Open(file_path)
                wordDoc.SaveAs(docx_file, FileFormat=fileFormate)
                wordDoc.Close()
        except Exception as e:
            print('Failed to Convert: {0}'.format(file_path))
            print(e.message)
    return fileName
    documentNew.Quit()


def extract_Embedded_File(file_path,save_path,extension):
    if(extension=='.doc' or extension=='xls' or extension=='ppt'):
        file_path = convert_OldMS(file_path, extension)
        extension = extension + 'x'

    if(extension=='.pptx'):
        extract_Embeddings_from_pptx(file_path)
        return

    subdir = {
        '.xlsx': 'xl',
        '.xlsm': 'xl',
        '.xltx': 'xl',
        '.xltm': 'xl',
        '.docx': 'word',
        '.dotx': 'word',
        '.docm': 'word',
        '.doc': 'word',
        '.dotm': 'word',

    }

    s_d = subdir.get(extension)
    temp_dir = tempfile.mkdtemp()
    # extract contents  file to temporary dir
    zip_file = zipfile.ZipFile(file_path,'r')
    zip_file.extractall(temp_dir)
    zip_file.close()

    # find all embedded files and copy to save_path
    embeddings_dir = f'{temp_dir}/{s_d}/embeddings/'
    embedded_files = list(glob.glob(embeddings_dir+'*'))
    #display the embedded files
    print(embedded_files)
    #save a copy of the embedded files
    for file in embedded_files:
        extensions = os.path.splitext(file)[1]
        if extensions == ".bin" or extensions == ".xml":
            continue
        shutil.copy(file, save_path)

#create a folder to save the embedded files
def createFoder(folder_name) -> bool:
    if not os.path.exists(folder_name):
        os.makedirs(folder_name)
        print(f"{folder_name} created successfully")
        return True
    else:
        print(f"{folder_name} already exists")
        return False

# files=['A-DOC-file-without-attachments-but-with-embeddings.doc', 'book1.xls', 'SDP.pptx']
input_dir_path = os.listdir("input")
files = [f for f in input_dir_path if os.path.isfile(os.path.join("input", f))]
for word_doc in files:
        extension = os.path.splitext(word_doc)[1]
        time = datetime.datetime.now().strftime("%Y_%m_%d_%H:%M:%S")
        save_folder_path = 'embedded_files_'+word_doc+"_"+time
        createFoder(save_folder_path)
        extract_Embedded_File(word_doc, save_folder_path,extension)
